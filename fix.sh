#!/usr/bin/env bash
find src \( -name CMakeLists.txt -o -name "*.cmake" \) \
    -exec sed -i -e 's/CMAKE_SOURCE_DIR/PROJECT_SOURCE_DIR/g' {} \; \
    -exec sed -i -e 's/CMAKE_BINARY_DIR/PROJECT_BINARY_DIR/g' {} \; \
    -exec sed -i -e 's/set \{0,1\}(CMAKE_MODULE_PATH/list(APPEND CMAKE_MODULE_PATH/g' {} \; \
    -exec sed -i -e 's/feature_summary \{0,1\}(/#feature_summary(/g' {} \; \
    -exec sed -i -e 's/export(EXPORT KF5DocTools/#export(EXPORT KF5DocTools/g' {} \;


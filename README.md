WIP kdesrc-build alternative

Design:
- CMake ExternalProject (https://cmake.org/cmake/help/latest/module/ExternalProject.html)
- mkosi (https://github.com/systemd/mkosi)

With ExternalProject, we clone, configure, build and install everything in the
correct order. With mkosi, we build an entire operating system image that we can
boot using qemu/kvm.

Just like buildroot, we add 'project'-rebuild, 'project'-reconfigure, etc
commands that manually delete ExternalProject stamp files to rebuild a specific
project that's being worked on. By default, we clone from HEAD and disable
updates unless specifically asked for. ExternalProject has a REBASE option that
can be used to save any changes on a local branch, update master and rebase on
top of it.

All this combined should (when finished) provide a simple, self-contained way to
work on KDE.
